import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'dart:io';
import 'package:molpay_mobile_xdk_flutter_beta/molpay_mobile_xdk_flutter_beta.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Token _paymentToken;
  PaymentMethod _paymentMethod;
  String _error;
  final String _currentSecret = null; //set this yourself, e.g using curl
  PaymentIntentResult _paymentIntent;
  Source _source;

  ScrollController _controller = ScrollController();

  final CreditCard testCard = CreditCard(
    number: '4000002760003184',
    expMonth: 12,
    expYear: 21,
  );

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  initState() {
    super.initState();

    StripePayment.setOptions(
        StripeOptions(publishableKey: "pk_test_aSaULNS8cJU6Tvo20VAXy6rp", merchantId: "Test", androidPayMode: 'test'));
  }

  void setError(dynamic error) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(error.toString())));
    setState(() {
      _error = error.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    //   home: Scaffold(body: Container(),),
    // );
    return new MaterialApp(
      home: new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text('Plugin example app'),
        ),
        body: ListView(
          controller: _controller,
          padding: const EdgeInsets.all(20),
          children: <Widget>[
            // RaisedButton(
            //   child: Text("Create Source"),
            //   onPressed: () {
            //     StripePayment.createSourceWithParams(SourceParams(
            //       type: 'ideal',
            //       amount: 1099,
            //       currency: 'eur',
            //       returnURL: 'example://stripe-redirect',
            //     )).then((source) {
            //       _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Received ${source.sourceId}')));
            //       setState(() {
            //         _source = source;
            //       });
            //     }).catchError(setError);
            //   },
            // ),
            // Divider(),
            // RaisedButton(
            //   child: Text("Create Token with Card Form"),
            //   onPressed: () {
            //     StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest()).then((paymentMethod) {
            //       _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Received ${paymentMethod.id}')));
            //       setState(() {
            //         _paymentMethod = paymentMethod;
            //       });
            //     }).catchError(setError);
            //   },
            // ),
            // RaisedButton(
            //   child: Text("Create Token with Card"),
            //   onPressed: () {
            //     StripePayment.createTokenWithCard(
            //       testCard,
            //     ).then((token) {
            //       _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Received ${token.tokenId}')));
            //       setState(() {
            //         _paymentToken = token;
            //       });
            //     }).catchError(setError);
            //   },
            // ),
            // Divider(),
            // RaisedButton(
            //   child: Text("Create Payment Method with Card"),
            //   onPressed: () {
            //     StripePayment.createPaymentMethod(
            //       PaymentMethodRequest(
            //         card: testCard,
            //       ),
            //     ).then((paymentMethod) {
            //       _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Received ${paymentMethod.id}')));
            //       setState(() {
            //         _paymentMethod = paymentMethod;
            //       });
            //     }).catchError(setError);
            //   },
            // ),
            // RaisedButton(
            //   child: Text("Create Payment Method with existing token"),
            //   onPressed: _paymentToken == null
            //       ? null
            //       : () {
            //           StripePayment.createPaymentMethod(
            //             PaymentMethodRequest(
            //               card: CreditCard(
            //                 token: _paymentToken.tokenId,
            //               ),
            //             ),
            //           ).then((paymentMethod) {
            //             _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Received ${paymentMethod.id}')));
            //             setState(() {
            //               _paymentMethod = paymentMethod;
            //             });
            //           }).catchError(setError);
            //         },
            // ),
            // Divider(),
            // RaisedButton(
            //   child: Text("Confirm Payment Intent"),
            //   onPressed: _paymentMethod == null || _currentSecret == null
            //       ? null
            //       : () {
            //           StripePayment.confirmPaymentIntent(
            //             PaymentIntent(
            //               clientSecret: _currentSecret,
            //               paymentMethodId: _paymentMethod.id,
            //             ),
            //           ).then((paymentIntent) {
            //             _scaffoldKey.currentState
            //                 .showSnackBar(SnackBar(content: Text('Received ${paymentIntent.paymentIntentId}')));
            //             setState(() {
            //               _paymentIntent = paymentIntent;
            //             });
            //           }).catchError(setError);
            //         },
            // ),
            // RaisedButton(
            //   child: Text("Authenticate Payment Intent"),
            //   onPressed: _currentSecret == null
            //       ? null
            //       : () {
            //           StripePayment.authenticatePaymentIntent(clientSecret: _currentSecret).then((paymentIntent) {
            //             _scaffoldKey.currentState
            //                 .showSnackBar(SnackBar(content: Text('Received ${paymentIntent.paymentIntentId}')));
            //             setState(() {
            //               _paymentIntent = paymentIntent;
            //             });
            //           }).catchError(setError);
            //         },
            // ),
            // Divider(),
            RaisedButton(
              child: Text("Native payment"),
              onPressed: () {
                if (Platform.isIOS) {
                  _controller.jumpTo(450);
                }
                StripePayment.paymentRequestWithNativePay(
                  androidPayOptions: AndroidPayPaymentRequest(
                    total_price: "1.20",
                    currency_code: "MYR",
                  ),
                  applePayOptions: ApplePayPaymentOptions(
                    countryCode: 'MY',
                    currencyCode: 'MYR',
                    items: [
                      ApplePayItem(
                        label: 'Test',
                        amount: '13',
                      )
                    ],
                  ),
                ).then((token) {
                  setState(() {
                    print(token);
                    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Received ${token.tokenId}')));
                    _paymentToken = token;
                  });
                }).catchError(setError);
              },
            ),
            RaisedButton(
              child: const Text('Start Molpay'),
              onPressed: () async {
                var paymentDetails = {
                  // Mandatory String. A value more than '1.00'
                  'mp_amount': '1.1',

                  // Mandatory String. Values obtained from MOLPay
                  'mp_username': 'api_fatehub',
                  'mp_password': 'api_hBIWhub78',
                  'mp_merchant_ID': 'fatehub',
                  'mp_app_name': 'MLTV',
                  'mp_verification_key': '551986d2f2f5afa45e20bb784791b4b2',

                  // Mandatory String. Payment values
                  'mp_order_ID': 'Flutter0002',
                  'mp_currency': 'MYR',
                  'mp_country': 'MY',

                  // Optional String.
                  'mp_channel':
                      '', // Use 'multi' for all available channels option. For individual channel seletion, please refer to "Channel Parameter" in "Channel Lists" in the MOLPay API Spec for Merchant pdf.
                  'mp_bill_description': 'Flutter payment test',
                  'mp_bill_name': 'anyname',
                  'mp_bill_email': 'example@email.com',
                  'mp_bill_mobile': '0161111111',
                  // 'mp_channel_editing': true, // Option to allow channel selection.
                  'mp_editing_enabled':
                      true, // Option to allow billing information editing.

                  // Optional for Escrow
                  // 'mp_is_escrow': '', // Optional for Escrow, put "1" to enable escrow

                  // Optional for credit card BIN restrictions
                  // 'mp_bin_lock': ['414170', '414171'], // Optional for credit card BIN restrictions
                  // 'mp_bin_lock_err_msg': 'Only UOB allowed', // Optional for credit card BIN restrictions

                  // For transaction request use only, do not use this on payment process
                  // 'mp_transaction_id': '', // Optional, provide a valid cash channel transaction id here will display a payment instruction screen.
                  // 'mp_request_type': '', // Optional, set 'Status' when doing a transactionRequest

                  // Optional, use this to customize the UI theme for the payment info screen, the original XDK custom.css file is provided at Example project source for reference and implementation.
                  // 'mp_custom_css_url': '',

                  // Optional, set the token id to nominate a preferred token as the default selection, set "new" to allow new card only
                  // 'mp_preferred_token': '',

                  // Optional, credit card transaction type, set "AUTH" to authorize the transaction
                  // 'mp_tcctype': '',

                  // Optional, set true to process this transaction through the recurring api, please refer the MOLPay Recurring API pdf
                  // 'mp_is_recurring': false,

                  // Optional for channels restriction
                  // 'mp_allowed_channels': ['credit','credit3'],

                  // Optional for sandboxed development environment, set boolean value to enable.
                  'mp_sandbox_mode': true,

                  // Optional, required a valid mp_channel value, this will skip the payment info page and go direct to the payment screen.
                  // 'mp_express_mode': false,
                  // 'mp_bill_description_edit_disabled': false,
                  // 'mp_timeout' : 300,
                };

                String result = await molpay.startMolpay(paymentDetails);
                print("Result" + result);
              },
            ),
            // RaisedButton(
            //   child: Text("Complete Native Payment"),
            //   onPressed: () {
            //     StripePayment.completeNativePayRequest().then((_) {
            //       _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Completed successfully')));
            //     }).catchError(setError);
            //   },
            // ),
            // Divider(),
            // Text('Current source:'),
            // Text(
            //   JsonEncoder.withIndent('  ').convert(_source?.toJson() ?? {}),
            //   style: TextStyle(fontFamily: "Monospace"),
            // ),
            // Divider(),
            // Text('Current token:'),
            // Text(
            //   JsonEncoder.withIndent('  ').convert(_paymentToken?.toJson() ?? {}),
            //   style: TextStyle(fontFamily: "Monospace"),
            // ),
            // Divider(),
            // Text('Current payment method:'),
            // Text(
            //   JsonEncoder.withIndent('  ').convert(_paymentMethod?.toJson() ?? {}),
            //   style: TextStyle(fontFamily: "Monospace"),
            // ),
            // Divider(),
            // Text('Current payment intent:'),
            // Text(
            //   JsonEncoder.withIndent('  ').convert(_paymentIntent?.toJson() ?? {}),
            //   style: TextStyle(fontFamily: "Monospace"),
            // ),
            // Divider(),
            // Text('Current error: $_error'),
          ],
        ),
      ),
    );
  }
}
